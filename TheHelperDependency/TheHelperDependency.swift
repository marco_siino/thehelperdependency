//
//  TheHelperDependency.swift
//  TheHelperDependency
//
//  Created by Marco Siino on 22/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

import Foundation
import AwesomeDependency

public class AwesomeHelper {
    let awesome = AwesomeDependency()
    
    public init() { }
    
    public func awesomeMethod(str: String) -> String {
        return awesome.awesomeMethod(str: str, doubleAwesome: true)
    }
}
